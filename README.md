ARN-GRAV
========

## Goal
The goal of this repository is to give the tools to easily develop and deploy static pages for [arn-fai.net](https://arn-fai.net).  
There's no real graphical charter at the time of this writing. I'm going to use what's already written in [this pdf](assets/charte_graphique_arn.pdf).

The themes must be flexible, simple and use technologies as stable as possible.  
We all know that these static pages wont be updated often and content will be produced by volonteers without time or technical skills.

## Stack choice
The backend framework is [Grav](http://getgrav.org/).  
Docker can be used to easily setup the dev's env.  
To create the theme, bootstrap was chosen as it is well-known, modulable and offers a great deal of customization.

## Yarn
To simplify the process of compiling the frontend, we use yarn to get the modules and compile them.
```
cd /user/themes/arn
yarn install
yarn build
```

## Docker
To build the grav image:
```
sudo docker build -t grav docker/
```

To use the container:
```
sudo docker run --rm --name grav -it -e UID=$(id -u) -e GID=$(id -g) -p 80:8080 -v $(pwd)/user/:/grav/user grav
```

To spawn a shell inside the container (without using root):
```
sudo docker exec -it grav gosu "$(id -u):$(id -g)" bash
```
