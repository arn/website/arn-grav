---
title: Nous joindre
icon:
    id: group
    color: '#334d63'
---
Local : [réunion mensuelle ouverte](#)  
[Liste de discussion](#) : [discussion CHEZ listes.arn-fai.net](#)  
IRC : [#arn sur irc.geeknode.org:6697](#)  
XMPP : [arn@conference.rows.io](#)  
Mail : [contact](#)  
