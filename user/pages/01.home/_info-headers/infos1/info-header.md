---
title: Solidaire
icon:
    id: group
    color: '#334d63'
---
Nous travaillons en équipe, et il y en a pour toutes et tous !
Pas besoin d'être administrateur système pour rejoindre l'association.
