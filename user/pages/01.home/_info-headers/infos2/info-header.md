---
title: Évolutif
icon:
    id: sync
    color: '#334d63'
---
Plus de membres → plus de moyens → meilleur service.  
Services actuellement en place : [VPS](#), [VPN](#), [hébergement](#), [accès internet sans-fil](#).  
En projet : raccordement WiFi, ADSL.
