---
icon:
  id: 'database'
  color: '#334d63'
title: 'VPS "STO-2G-200"'
action:
  href: '#'
  text: 'Souscrire'
items:
  - 2vcpu
  - 2Go de RAM
  - 200Go HDD
  - 1 adresse IPv4
  - 1 adresse IPv6
  - 12€/mois
---
