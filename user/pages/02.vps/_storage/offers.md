---
title: 'Nos offres "stockage"'

content:
    items: '@self.children'
---
Nos VPS STO offrent une solution avantageuse pour mettre en place votre propre service de stockage (cloud personnel, système de sauvegarde,...). Il est possible de demander une extension de votre espace de stockage pour +3€/mois par tranche de +200Go; et une adresse IP supplémentaire pour 4€/mois.
