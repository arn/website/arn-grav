---
icon:
  id: 'database'
  color: '#334d63'
title: 'VPS "STO-4G-400"'
action:
  href: '#'
  text: 'Souscrire'
items:
  - 4vcpu
  - 4Go de RAM
  - 400Go HDD
  - 1 adresse IPv4
  - 1 adresse IPv6
  - 24€/mois
---
