---
icon:
  id: 'database'
  color: '#334d63'
title: 'VPS "STO-1G-200"'
action:
  href: '#'
  text: 'Souscrire'
items:
  - 1vcpu
  - 1Go de RAM
  - 200Go HDD
  - 1 adresse IPv4
  - 1 adresse IPv6
  - 10€/mois
---
