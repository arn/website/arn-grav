---
icon:
    id: 'rocket'
    color: '#334d63'
title: 'VPS "SSD-2G-100"'
action:
    href: '#'
    text: 'Souscrire'
items:
  - 2vcpu
  - 2Go de RAM
  - 100Go SSD
  - 1 adresse IPv4
  - 1 adresse IPv6
  - 20€/mois
---
