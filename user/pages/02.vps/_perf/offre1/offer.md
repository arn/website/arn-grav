---
icon:
  id: 'rocket'
  color: '#334d63'
title: 'VPS "SSD-1G-50"'
action:
  href: '#'
  text: 'Souscrire'
items:
  - 1vcpu
  - 1Go de RAM
  - 50Go SSD
  - 1 adresse IPv4
  - 1 adresse IPv6
  - 10€/mois
---
