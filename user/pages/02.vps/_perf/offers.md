---
title: 'Nos offres "performances"'

content:
    items: '@self.children'
---
Les VPS SSD privilégient la performance grâce à l'utilisation de disques SSD en grappe raid6 (distribution et réplication des données) qui permet de meilleurs temps d'accès. Il est possible d'obtenir une adresse IP supplémentaire pour 4€/mois.
