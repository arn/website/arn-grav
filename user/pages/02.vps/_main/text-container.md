**Alsace Réseau Neutre**, votre fournisseurs d'accès associatif, vous propose d'héberger vos serveurs virtuels au sein de son infrastructure.  
Vos serveurs profitent ainsi des conditions (connexions, débits, stabilité, ...) présentes dans un datacenter tout en participant à la construction d'un internet respectueux de ses usagers.  
En devenant adhérant vous obtenez aussi un droit de vote sur les décisions prises par l'association et rejoignez une équipe de militant-e-s passionné-e-s.

L'objectif de ce service est de vous proposer un espace d'hébergement **totalement personnalisable** sans que vous ayez à vous soucier de la gestion du matériel sur lequel il repose (achat, réparation, maintenance, nettoyage, etc.) puisque cette charge incombe à l'association.

Un minimum de compétences (ou d'envie de les acquérir) en administration d'un serveur informatique est requis. Avec un VPS, rien n'est pré-configuré ni clé en main.  
À vous de construire brique après brique le service dont vous avez besoin, aucun-e technicien-ne de l'association ne gère votre VPS à votre place (mais les abonné-e-s s'entraident) : **votre autonomie est maximale**.

ARN propose deux gammes de VPS:
- les **VPS SSD**, qui privilégient la performance (disque SSD en grappe raid6),
- les **VPS STOCKAGE**, qui privilégient le stockage.
