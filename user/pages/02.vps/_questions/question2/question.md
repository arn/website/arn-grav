---
title: Qu'est ce que je peux faire avec mon VPS ?
---
Chaque adonné-e VPS devient administrateur/administratrice de sa machine virtuelle avec son espace disque, sa quantité de mémoire et de processeur, ainsi que sa propre adresse IP. Très utile pour les personnes désirant découvrir et apprendre comment on administre un serveur informatique sans investir ni disposer du matériel.

En tant qu'administrateur/administratrice de votre VPS vous pouvez :
- Installer les logiciels de votre choix ;
- Paramétrer votre système et vos logiciels dans les moindres détails ;
- Diffuser tous les services et tous les contenus que vous voulez (votre site web, vos mails, vos photos, etc.) en profitant de sa bonne connectivité à Internet.

L'objectif de ce service est de vous proposer un espace d'hébergement totalement personnalisable sans que vous ayez à vous soucier de la gestion du matériel sur lequel il repose (achat, réparation, maintenance, nettoyage, etc.) puisque cette charge incombe à l'association.

Un minimum de compétences (ou d'envie de les acquérir) en administration d'un serveur informatique est requis. Avec un VPS, rien n'est pré-configuré ni clé en main, à vous de construire brique après brique le service dont vous avez besoin, aucun-e technicien-ne de l'association ne gère votre VPS à votre place (mais les abonné-e-s s'entraident) : autonomie maximale.

Attention : ARN est une association située en France et nous respectons la législation en vigueur. Ainsi, nous donnerons suite à toute demande d'identification justifiée par la loi. Autrement dit, si votre objectif est de vous cacher d'HADOPI (autorité indépendante chargée de lutter contre la diffusion d'œuvres sans l'accord de leurs auteurs) ou de mettre en place un site web présentant un contenu interdit par la loi française, ne prenez pas un VPS chez ARN. Ce sont là des exemples parmi d'autres, toute la législation française s'impose à vous et à ARN.
