---
title: Est-il possible de faire une installation personnalisée du système d'exploitation ?
---
Oui, à votre demande. Cela vous permet d'activer le chiffrement de votre disque dur, par exemple. À défaut, la configuration est celle de l'installation standard (une seule partition non chiffrée, système en anglais, etc.).
