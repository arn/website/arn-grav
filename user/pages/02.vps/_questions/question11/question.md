---
title: Est-ce qu'ARN conserve des informations / traces / logs ?
---
Oui, tel que requis par la législation française : votre identité (prénom + nom + adresse postale + numéro de téléphone) + les adresses IP assignées par ARN à votre VPS. Pas plus, pas moins. Tout cela est conservé pendant 1 an. ARN se réserve le droit de vérifier la véracité de votre identité.

