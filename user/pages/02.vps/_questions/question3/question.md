---
title: Est-il possible de faire évoluer mon offre ?
---
Il est possible de passer à une offre supérieure tant qu'on reste dans la même gamme (STO ou SSD). Le passage à une offre inférieure n'est pas possible en ce qui concerne la quantité de SSD ou HDD.

Nos ressources (stockage, RAM, CPU) sont limitées. Nous prônons un usage optimal et responsable des ressources ainsi que la solidarité entre les membres de l'association, pas l'accaparation de toutes les ressources par quelques un-e-s ni le gaspillage. Dans ce contexte, si vous savez que vous n'utiliserez pas une des ressources de votre offre, vous pouvez l'indiquer lors de la création de votre VPS afin que l'administrateur dimensionne votre VPS en fonction.

Vous pouvez demander une adresse IP supplémentaire pour 4€/mois quelque soit votre offre.

Pour nos offres VPS STO, il est possible d'augmenter votre espace de stockage pour +3€/mois par tranche de +200Go supplémentaire.
