---
title: Modalités de paiement
---
Le mois de la souscription est un mois d'essai. Le premier paiement est donc attendu au plus tard dans la première quinzaine du mois suivant avec possibilité de régler jusqu'à 12 mois d'avance en une seule fois.

Le paiement est a effectué par virement bancaire. Les références bancaires se trouvent dans l'espace adhérent. N'oubliez pas d'indiquer « ID » + le numéro d'identifiant qui vous a été attribué, tel que demandé dans l'espace adhérant.
