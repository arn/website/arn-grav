---
title: Comment accéder à mon VPS ?
---
Deux possibilités.

Par SSH, accès en ligne de commande / texte. Nous préférons une authentification par clé afin d'éviter des attaques par bruteforce (force brute en français, essai systématique et automatique de toutes les combinaisons identifiant + mot de passe possibles afin de trouver celle que vous avez choisie). Une authentification par mot de passe reste possible, mais est vivement déconseillée. Depuis Stretch l'accès SSH par mot de passe a été désactivé.

Par VNC, affichage graphique à distance. Cela permet de se dépanner soi-même suite à la commission d'une erreur qui vous empêche d'avoir un accès SSH (mauvaise configuration du pare-feu, VPS qui ne redémarre pas comme prévu, etc.). C'est également cette fonctionnalité qui rend possible une installation personnalisée du système d'exploitation et la saisie de votre phrase de passe pour démarrer un système chiffré.
