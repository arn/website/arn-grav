---
title: C'est quoi un VPS ?
---
Un VPS est avant tout un serveur, c'est à dire une machine qui fonctionne et est connectée à Internet en permanence. Le but d'un serveur est de fournir un service (hébergement, stockage, ...).

Pour profiter de bonnes conditions (débit, latence, température, stabilité électrique... ). les serveurs sont généralement installés dans un datacenter. Mais les places sont chers, aussi afin de mutualiser les coûts il est possible de découper un serveur physique en plusieurs serveurs virtuels. Chaque serveur virtuel reçoit alors une partie des ressources (mémoire, processeur, stockage, ...) et est isolé des autres serveurs virtuels tournant sur la même machine.

Les avantages de la virtualisation sont multiples :
- Réduction des coûts et de l’encombrement grâce à la mutualisation
- Installation, déploiement facilité
- l'utilisateur n'a pas besoin de s'occuper de la couche matérielle
- migration possible en cas de problèmes matériels
- Isolation des serveurs entre eux
- Possibilité de faire évoluer les caractéristiques de son serveur en fonction de ses besoins

Ce service constitue une alternative aux offres d'hébergement mutualisé qui, certes, ne requièrent que très peu de compétences informatiques mais sont limitées : les services et les logiciels sont limités à ceux que le prestataire propose (parfois uniquement l'hébergement d'un site web) et la configuration est standardisée (pas personnalisable). Autrement dit, un VPS est à mi-chemin entre héberger un serveur physique chez soi ou dans l'association et un hébergement mutualisé.
