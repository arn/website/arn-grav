---
title: Quels systèmes d'exploitation sont disponibles ?
---
Par défaut, Debian GNU/Linux stable x86-64. Installation automatique standardisée (seuls les utilitaires de base et un serveur SSH sont installés).

À votre demande, tout système libre sur architecture x86/x86-64. Exemples : Debian, Ubuntu Server, CentOS, OpenBSD, FreeBSD, etc. L'installation est alors entièrement à votre charge (elle n'est ni automatique, ni clé en main).
