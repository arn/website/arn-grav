---
title: Est-ce que ARN sauvegarde automatiquement mon VPS ?
---
NON. La sauvegarde de vos données et de vos configurations présentes sur votre VPS est entièrement à votre charge.
