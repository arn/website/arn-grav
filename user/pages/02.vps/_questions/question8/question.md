---
title: Comment ça marche ? (Technologies utilisées )
---
KVM pour l'hyperviseur ; Ganeti pour la redondance : en cas de panne matérielle sur l'un de nos serveurs, votre VPS sera transféré sur un autre serveur. De plus, aucune interruption de service à prévoir lors d'une maintenance de notre côté.
