---
title: Reverse IP personnalisables ?
---
Oui.

Pour l'IPv4, il faut nous indiquer le nom désiré.

Pour l'IPv6, soit vous nous indiquez le nom désiré pour une ou plusieurs adresses, soit nous vous déléguons la zone qui correspond à votre /56 afin que vous gériez vous-même les reverses associés à vos adresses. La deuxième méthode suppose que vous disposez d'au moins un serveur de noms qui fait autorité.
