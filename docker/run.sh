#!/bin/sh
set -xe

# copy default grav user files if system.yaml doesn't exists
if ! [ -e /grav/user/config/system.yaml ]; then
    cp -r /grav/user_orig/* /grav/user/
fi

mkdir -p /run/nginx /run/php
chown -R "$UID:$GID" /grav /etc/php /etc/nginx /etc/supervisor /var/lib/nginx /var/log/ /var/tmp/ /run/nginx /var/run/
exec gosu "$UID:$GID" supervisord -c /etc/supervisor/supervisord.conf --nodaemon
